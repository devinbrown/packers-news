import urllib2
from bs4 import BeautifulSoup
from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route("/")
def hello():
	headline = getHtml('https://www.packersnews.com')
	return render_template('index.html', titles = headline)

def getHtml(url):
	page = urllib2.urlopen(url)
	soup = BeautifulSoup(page, 'html.parser')

	headline = soup.findAll('span', attrs={'itemprop': 'headline'})
	return headline
